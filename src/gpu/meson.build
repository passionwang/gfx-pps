# Copyright (c) 2020 Collabora, Ltd.
# Author: Antonio Caggiano <antonio.caggiano@collabora.com>
#
# SPDX-License-Identifier: MIT

# Common GPU sources, includes, and libraries to link
dep_drm = dependency('libdrm')

gpu_deps = [dep_perfetto]

subdir('panfrost')
subdir('intel')

gpu_sources = [
	'drm_device.cc',
	'driver.cc',
	'counters.cc',
]

lib_pps_gpu = static_library(
	'pps-gpu',
	sources: gpu_sources,
	dependencies: gpu_deps
)

dep_pps_gpu = declare_dependency(
	link_with: lib_pps_gpu,
	include_directories: include_pps
)

gpu_producer_sources = [
	'gpu_datasource.cc',
	'main.cc'
]

# GPU counters data-source
executable(
	'producer-gpu',
	sources: gpu_producer_sources,
	include_directories: include_pps,
	dependencies: dep_pps_gpu,
	install: true
)
