/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 * Author: Rohan Garg <rohan.garg@collabora.com>
 * Author: Robert Beckett <bob.beckett@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "pps/gpu/driver.h"
#include "pps/gpu/panfrost/panfrost_driver.h"
#include "pps/gpu/intel/intel_driver.h"

#include <pps/pds.h>

namespace pps::gpu
{
const std::vector<std::string> &Driver::supported_device_names()
{
    static std::vector<std::string> supported_device_names = {
        PanfrostDriver::get_name(),
        IntelDriver::get_name()};
    return supported_device_names;
}

std::unique_ptr<Driver> Driver::create(DrmDevice &&drm_device)
{
    std::unique_ptr<Driver> driver;

    if (drm_device.name == PanfrostDriver::get_name()) {
        driver = std::make_unique<PanfrostDriver>();
    }
    else if (drm_device.name == IntelDriver::get_name()) {
        driver = std::make_unique<IntelDriver>();
    }

    if (!driver) {
        PERFETTO_ELOG("Failed to find a driver for DRM device %s", drm_device.name.c_str());
        return nullptr;
    }

    driver->drm_device = std::move(drm_device);
    return driver;
}

} // namespace pps::gpu
