/*
 * Copyright © 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <i915-perf/perf.h>
#include <i915-perf/perf_data.h>

#include "pps/gpu/driver.h"

namespace pps::gpu
{
/// @brief Variable length sequence of bytes generated
/// by Intel Obstervation Architecture (OA)
using PerfRecord = std::vector<uint8_t>;

/// @brief Driver class for Intel graphics devices
class IntelDriver : public Driver
{
public:
    static const std::string &get_name();

    IntelDriver() = default;

    IntelDriver(IntelDriver &&);
    IntelDriver &operator=(IntelDriver &&);

    ~IntelDriver() override;

    std::optional<intel_perf_record_timestamp_correlation> query_correlation_timestamps() const;

    /// @brief OA reports only have the lower 32 bits of the timestamp
    /// register, while correlation data has the whole 36 bits.
    /// @param gpu_ts a 32 bit OA report GPU timestamp
    /// @return The CPU timestamp relative to the argument
    uint64_t correlate_gpu_timestamp(uint32_t gpu_ts);

    int perf_open(const intel_perf_metric_set &metric_set, uint64_t sampling_period_ns);

    uint64_t get_min_sampling_period_ns() override;
    bool init_perfcnt() override;
    void enable_counter(uint32_t counter_id) override;
    void enable_all_counters() override;
    void enable_perfcnt(uint64_t sampling_period_ns) override;
    void disable_perfcnt() override;
    bool dump_perfcnt() override;
    uint64_t next() override;

    uint64_t timestamp_frequency = 0;
    intel_perf *perf = nullptr;
    intel_perf_accumulator accu = {};

    /// This is used to correlate CPU and GPU timestamps
    std::array<intel_perf_record_timestamp_correlation, 64> correlations;

    /// List of OA perf records read so far
    std::vector<PerfRecord> records;

    intel_perf_metric_set *metric_set = nullptr;
    /// Stream file descriptor for a configured metric set
    int metric_fd = -1;
};

} // namespace pps::gpu
