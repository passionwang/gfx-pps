/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 * Author: Robert Beckett <bob.beckett@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <pps/gpu/driver.h>
#include <pps/pds.h>

namespace pps::gpu
{
struct GpuIncrementalState {
    bool was_cleared = true;
};

struct GpuDataSourceTraits : public perfetto::DefaultDataSourceTraits {
    using IncrementalStateType = GpuIncrementalState;
};

class Driver;

class GpuDataSource : public perfetto::DataSource<GpuDataSource, GpuDataSourceTraits>
{
public:
    void OnSetup(const SetupArgs &args) override;
    void OnStart(const StartArgs &args) override;
    void OnStop(const StopArgs &args) override;

    /// @brief Perfetto trace callback
    static void trace_callback(TraceContext ctx);
    static void RegisterDataSource();

    void trace(TraceContext &ctx, uint64_t sampling_period_ns);

private:
    State state = State::Stop;
#define GPU_DATASOURCE_DEFAULT_SAMPLING_PERIOD 500000
    uint64_t sampling_period_ns = GPU_DATASOURCE_DEFAULT_SAMPLING_PERIOD;
    perfetto::base::TimeNanos time_to_trace;

    std::vector<std::unique_ptr<Driver>> drivers;
    
    // Timestamp of packet sent with counter descriptors
    uint64_t descriptor_timestamp = 0;

};

} // namespace pps::gpu
