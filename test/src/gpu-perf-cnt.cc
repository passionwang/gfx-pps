#include <pps/gpu/driver.h>

#include <cstdlib>
#include <cstring>
#include <docopt/docopt.h>
#include <thread>

static const char *USAGE =
    R"(gpu-perf-cnt

  Usage:
	gpu-perf-cnt info
	gpu-perf-cnt dump [--gpu=<n>] [--sec=<n>]
	gpu-perf-cnt groups [--gpu=<n>]
	gpu-perf-cnt counters [--gpu=<n>]
	gpu-perf-cnt (-h | --help)
	gpu-perf-cnt --version

  Options:
	-h --help  Show this screen.
	--version  Show version.
	--gpu=<n>  GPU number to query [default: 0].
	--sec=<n>  Seconds to wait before dumping performance counters [default: 0].
)";

// Tool running mode
enum class Mode {
    // Show help message
    Help,

    // Show system information
    Info,

    // Show list of available counters
    Counters,

    // Groups
    Groups,

    // Dump performance counters
    Dump,
};

int main(int argc, const char **argv)
{
    using namespace pps;

    Mode mode = Mode::Help;
    auto secs = std::chrono::seconds(0);
    uint32_t gpu_num = 0;

    auto args =
        docopt::docopt(USAGE, {std::next(argv), std::next(argv, argc)}, true, "gpu-perf-cnt 0.2");

    if (args["info"].asBool()) {
        mode = Mode::Info;
    }

    if (args["dump"].asBool()) {
        mode = Mode::Dump;
    }

    if (args["--gpu"]) {
        gpu_num = static_cast<uint32_t>(args["--gpu"].asLong());
    }

    if (args["--sec"]) {
        secs = std::chrono::seconds(args["--sec"].asLong());
    }

    if (args["groups"].asBool()) {
        mode = Mode::Groups;
    }

    if (args["counters"].asBool()) {
        mode = Mode::Counters;
    }

    // Docopt shows the help message for us
    if (mode == Mode::Help) {
        return EXIT_SUCCESS;
    }

    switch (mode) {
    default:
        break;
    case Mode::Info: {
        // Header: device name, and whether it is supported or not
        printf("#%4s %16s %16s\n", "num", "device", "support");

        auto devices = gpu::DrmDevice::create_all();
        for (auto &device : devices) {
            auto gpu_num = device.gpu_num;
            auto name = device.name;
            auto driver = gpu::Driver::create(std::move(device));
            printf(" %4u %16s %16s\n", gpu_num, name.c_str(), driver ? "yes" : "no");
        }

        break;
    }
    case Mode::Dump: {
        if (auto device = gpu::DrmDevice::create(gpu_num)) {
            if (auto driver = gpu::Driver::create(std::move(device.value()))) {
                driver->init_perfcnt();
                driver->enable_perfcnt(std::chrono::nanoseconds(secs).count());
                std::this_thread::sleep_for(std::chrono::seconds(secs));

                driver->dump_perfcnt();
                std::fwrite(driver->samples.data(),
                    sizeof(driver->samples[0]),
                    driver->samples.size(),
                    stdout);
            }
        }
        break;
    }
    case Mode::Groups: {
        if (auto device = gpu::DrmDevice::create(gpu_num)) {
            if (auto driver = gpu::Driver::create(std::move(device.value()))) {
                driver->init_perfcnt();
                printf("#%4s %32s\n", "id", "name");

                for (auto &group : driver->groups) {
                    printf(" %4u %32s\n", group.id, group.name.c_str());
                }
            }
        }

        break;
    }
    case Mode::Counters: {
        if (auto device = gpu::DrmDevice::create(gpu_num)) {
            if (auto driver = gpu::Driver::create(std::move(device.value()))) {
                driver->init_perfcnt();
                printf("#%4s %32s\n", "id", "name");

                for (uint32_t i = 0; i < driver->counters.size(); ++i) {
                    auto &counter = driver->counters[i];
                    printf(" %4u %32s\n", counter.id, counter.name.c_str());
                }
            }
        }

        break;
    }
    } // switch

    return EXIT_SUCCESS;
}
