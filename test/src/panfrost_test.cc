/*
 * Copyright (c) 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <cstring>
#include <gtest/gtest.h>
#include <pps/gpu/panfrost/hwc_names.h>
#include <pps/gpu/panfrost/panfrost_driver.h>

#include <pps/algorithm.h>

namespace pps::gpu
{
#define GET_COUNTER_NAMES(b) std::vector<const char *>(b, b + sizeof(b) / sizeof(b[0]))

PanfrostDriver create_mali_t604()
{
    auto driver = PanfrostDriver();
    driver.cores = 1;
    driver.samples.resize(64 * (driver.cores + 3));
    auto counter_names = GET_COUNTER_NAMES(mali_userspace::hardware_counters_mali_t60x);
    return driver;
}

PanfrostDriver create_mali_t880()
{
    auto driver = PanfrostDriver();
    driver.cores = 1;
    driver.samples.resize(64 * (driver.cores + 3));
    auto counter_names = GET_COUNTER_NAMES(mali_userspace::hardware_counters_mali_t88x);
    return driver;
}

PanfrostDriver create_mali_t860_mp4()
{
    auto driver = PanfrostDriver();
    driver.cores = 4;
    driver.samples.resize(64 * (driver.cores + 3));
    auto counter_names = GET_COUNTER_NAMES(mali_userspace::hardware_counters_mali_t86x);
    std::tie(driver.groups, driver.counters) =
        PanfrostDriver::create_available_counters(counter_names);
    return driver;
}

void test_counters(const PanfrostDriver &driver)
{
    for (uint32_t i = 1; i < driver.counters.size(); ++i) {
        const Counter &counter = driver.counters[i];
        if (!counter.derived) {
            EXPECT_NE(0, counter.offset);
        }
    }
}

TEST(Panfrost, Counter)
{
    test_counters(create_mali_t604());
    test_counters(create_mali_t860_mp4());
    test_counters(create_mali_t880());
}

TEST(Panfrost, CountersCount)
{
    EXPECT_EQ(PanfrostDriver::query_counters_count(4), 1792 / sizeof(uint32_t));
}

TEST(Panfrost, PerfCnt)
{
    auto dump_path = "test/data/gpu/panfrost/perfcnt.dump";
    auto dump_file = std::fopen(dump_path, "rb");
    EXPECT_TRUE(dump_file != nullptr);

    std::fseek(dump_file, 0, SEEK_END);
    size_t dump_size = std::ftell(dump_file);
    std::fseek(dump_file, 0, SEEK_SET);
    EXPECT_EQ(dump_size, 1792);

    PanfrostDriver driver {create_mali_t860_mp4()};

    size_t counter_count = driver.samples.size();

    std::fread(driver.samples.data(), sizeof(uint32_t), counter_count, dump_file);

    auto it = FIND_IF(
        driver.counters, [](Counter &c) { return std::strstr(c.name.c_str(), "FRAG_ACTIVE"); });
    EXPECT_NE(it, std::end(driver.counters));
    Counter frag_active = *it;

    EXPECT_EQ(frag_active.group, static_cast<int32_t>(CounterBlock::SHADER_CORE));

    uint32_t block_index = to_u32(frag_active.group);
    size_t block_offset = frag_active.offset % PanfrostDriver::counters_per_block;

    // Check shader core 0
    size_t core0_offset = block_index * PanfrostDriver::counters_per_block + block_offset;
    EXPECT_LT(core0_offset, counter_count);
    uint32_t core0_value = driver.samples[core0_offset];
    EXPECT_EQ(core0_value, 91435465);

    // Check shader core 1
    size_t core1_offset = (block_index + 1) * PanfrostDriver::counters_per_block + block_offset;
    EXPECT_LT(core1_offset, counter_count);
    uint32_t core1_value = driver.samples[core1_offset];
    EXPECT_EQ(core1_value, 91439247);

    // Check shader core 2
    size_t core2_offset = (block_index + 2) * PanfrostDriver::counters_per_block + block_offset;
    EXPECT_LT(core2_offset, counter_count);
    uint32_t core2_value = driver.samples[core2_offset];
    EXPECT_EQ(core2_value, 91417873);

    // Check shader core 3
    size_t core3_offset = (block_index + 3) * PanfrostDriver::counters_per_block + block_offset;
    EXPECT_LT(core3_offset, counter_count);
    uint32_t core3_value = driver.samples[core3_offset];
    EXPECT_EQ(core3_value, 91409969);

    EXPECT_EQ(std::get<int64_t>(frag_active.get_value(driver)),
        core0_value + core1_value + core2_value + core3_value);
}

} // namespace pps::gpu

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
